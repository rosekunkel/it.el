;;; test-it.el --- Tests for It -*- lexical-binding: t; -*-
;;
;;; Commentary:
;;; Code:

(require 'buttercup)
(require 'it)


(describe "conversion functions"
  (describe "it-from"
    (it "leaves iterators unchanged"
      (let ((iter (funcall (iter-lambda () (iter-yield 1)))))
        (expect (it-from iter) :to-be iter)))
    (it "converts lists"
      (expect (it-collect 'list (it-from '(a b c))) :to-equal '(a b c)))
    (it "converts vectors"
      (expect (it-collect 'list (it-from [a b c])) :to-equal '(a b c)))
    (it "converts strings"
      (expect (it-collect 'list (it-from "abc")) :to-equal '(?a ?b ?c)))
    (it "errors on unknown types"
      (expect (it-from 123) :to-throw 'error)))

  (describe "it-collect"
    (it "creates lists"
      (expect (it-collect 'list (it-from '(a b c))) :to-equal '(a b c)))
    (it "creates vectors"
      (expect (it-collect 'vector (it-from '(a b c))) :to-equal [a b c]))
    (it "creates strings"
      (expect (it-collect 'string (it-from '(?a ?b ?c))) :to-equal "abc"))
    (it "errors on unknown types"
      (expect (it-collect 'foo iter-empty) :to-throw 'error))))

(describe "iterator transformers"
  (describe "it-map"
    (it "maps elements"
      (let ((result (it-collect 'list
                      (it-map #'upcase
                        (it-from '("a" "b" "c"))))))
        (expect result :to-equal '("A" "B" "C")))))

  (describe "it-flat-map"
    (it "maps and flattens"
      (let ((result (it-collect 'list
                      (it-flat-map #'it-from
                        (it-from '((a b c) (1 2 3)))))))
        (expect result :to-equal '(a b c 1 2 3))))

    (it "handles empty iterators"
      (let ((result (it-collect 'list
                      (it-flat-map #'it-from
                        (it-from '(() (a b c) () (1 2 3) ()))))))
        (expect result :to-equal '(a b c 1 2 3)))))

  (describe "it-filter"
    (it "filters elements"
      (let ((result (it-collect 'list
                      (it-filter #'integerp
                        (it-from '(a 1 b c 2 3 d e f))))))
        (expect result :to-equal '(1 2 3)))))

  (describe "it-flatten"
    (it "flattens"
      (let ((result (it-collect 'list
                      (it-flatten
                        (it-from (list (it-from '(a b c))
                                       (it-from '(1 2 3))))))))
        (expect result :to-equal '(a b c 1 2 3)))))

  (describe "it-take"
    (it "takes"
      (let ((result (it-collect 'list
                      (it-take 3
                        (it-from '(1 2 3 4 5))))))
        (expect result :to-equal '(1 2 3))))

    (it "handles short iterators"
      (let ((result (it-collect 'list
                      (it-take 3
                        (it-from '(1 2))))))
        (expect result :to-equal '(1 2)))))

  (describe "it-drop"
    (it "drops"
      (let ((result (it-collect 'list
                      (it-drop 3
                        (it-from '(1 2 3 4 5))))))
        (expect result :to-equal '(4 5))))
    (it "handles short iterators"
      (let ((result (it-collect 'list
                      (it-drop 3
                        (it-from '(1 2))))))
        (expect result :to-equal '()))))

  (describe "it-take-while"
    (it "takes while pred non-nil"
      (let ((result (it-collect 'list
                      (it-take-while #'integerp
                        (it-from '(1 2 a b c 3))))))
        (expect result :to-equal '(1 2)))))

  (describe "it-drop-while"
    (it "drops while pred non-nil"
      (let ((result (it-collect 'list
                      (it-drop-while #'integerp
                        (it-from '(1 2 a b c 3))))))
        (expect result :to-equal '(a b c 3))))))

(describe "it-zip-with"
  (it "zips with fn")
  (it "truncates to the shortest iterator")
  (it "handles no arguments"))

(describe "it-zip"
  (it "zips")
  (it "truncates to the shortest iterator")
  (it "handles no arguments"))

(describe "it-chain"
  (it "chains iterators")
  (it "handles no arguments"))

(describe "it-repeat"
  (it "repeats forever"))

(describe "it-iterate"
  (it "iterates fn"))

(describe "it-unfold"
  (it "unfolds")
  (it "stops on nil"))

;;; test-it.el ends here
