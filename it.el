;;; it.el --- A modern iterators library -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Rose Kunkel
;;
;; Author: Rose Kunkel <https://github.com/rose>
;; Maintainer: Rose Kunkel <rose@rosekunkel.me>
;; Created: March 09, 2021
;; Modified: March 09, 2021
;; Version: 0.0.1
;; Keywords: extensions, lisp, iterators, generators
;; Homepage: https://github.com/rose/it
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  A modern iterators library
;;
;;; Code:

(require 'generator)
(require 'cl-lib)
(require 'doc)

(defgroup it nil
  "Customize group for it.el, a modern iterator library."
  :group 'extensions
  :group 'lisp)

(defcustom it-from-functions
  '((functionp . identity)
    (listp . it--from-list)
    (arrayp . it--from-array))
  "Functions to convert Lisp objects to iterators.
Elements have the form (PRED . FUN). To convert OBJ to an
iterator, (FUN OBJ) is called on the first entry where (PRED OBJ)
returns non-nil."
  :type '(alist :key-type function :value-type function)
  :group 'it)

(doc-fun
    (defun it-from (obj)
      (let ((conversion-fun
             (cdr-safe (seq-find (lambda (converter) (funcall (car converter) obj))
                                 it-from-functions))))
        (if (functionp conversion-fun)
            (funcall conversion-fun obj)
          (error "Can't convert to iterator: %S" obj))))
  :public t
  :summary ("Convert " (arg obj) " to an iterator.")
  :description ("Uses " it-from-functions " to do the conversion.")
  :type (&-> any (iterator any)))

(doc-fun
    (iter-defun it--from-list (list)
      (cl-loop for elem in list do (iter-yield elem)))
  :summary ("Convert " (arg list) " to an iterator over its elements.")
  :type (&-> (list 'a) (iterator 'a)))

(doc-fun
    (iter-defun it--from-array (array)
      (cl-loop for elem across array do (iter-yield elem)))
  :summary ("Convert " (arg array) " to an iterator over its elements.")
  :type (&-> (array 'a) (iterator 'a)))

(defcustom it-collect-functions
  '((list . it--collect-list)
    (vector . it--collect-vector)
    (string . it--collect-string))
  "Functions to convert iterators into collections.
Elements have the form (TYPE . FUN), where TYPE is a symbol
usable as the first argument to `it-collect'."
  :type '(alist :key-type symbol :value-type function)
  :group 'it)

(doc-fun
    (defun it-collect (type iterator)
      (declare (indent 1))
      (let ((collection-fun (alist-get type it-collect-functions)))
        (if (functionp collection-fun)
            (funcall collection-fun iterator)
          (error "Not a valid collection type: %S" type))))
  :public t
  :summary
  ("Collect the elements of " (arg iterator) " into a sequence of type "
   (arg type) ".")
  :description
  ("Valid types are listed in " it-collect-functions ".")
  :type (&-> (&* (&: type symbol) (iterator 'a)) (type 'a)))

(doc-fun
    (defun it--collect-list (iterator)
      (let ((result '()))
        (iter-do (elem iterator)
          (push elem result))
        (nreverse result)))
  :summary ("Collect the elements of " (arg iterator) " into a list.")
  :type (&-> (iterator 'a) (list 'a)))

(doc-fun
    (defun it--collect-vector (iterator)
      (vconcat (it--collect-list iterator)))
  :summary ("Collect the elements of " (arg iterator) " into a vector.")
  :type (&-> (iterator 'a) (list 'a)))

(doc-fun
    (defun it--collect-string (iterator)
      (concat (it--collect-list iterator)))
  :summary ("Collect the elements of " (arg iterator) " into a string.")
  :type (&-> (iterator char) string))

(doc-fun
    (iter-defun it-map (fn iterator)
      (declare (indent 1))
      (iter-do (elt iterator)
        (iter-yield (funcall fn elt))))
  :public t
  :summary
  ("Yield " (arg fn) " applied to each value yielded by " (arg iterator) ".")
  :type (&-> (&* (&-> 'a 'b) (iterator 'a)) (iterator 'b)))

(doc-fun
    (iter-defun it-flat-map (fn iterator)
      (declare (indent 1))
      (iter-do (elt iterator)
        (iter-yield-from (funcall fn elt))))
  :public t
  :summary
  ("For each value yielded by " (arg iterator) ", apply " (arg fn)
   " to produce a new iterator, and then yield all its elements.")
  :type (&-> (&* (&-> 'a (iterator 'b)) (iterator 'a)) (iterator 'b)))

(doc-fun
    (iter-defun it-filter (pred iterator)
      (declare (indent 1))
      (iter-do (elt iterator)
        (when (funcall pred elt)
          (iter-yield elt))))
  :public t
  :summary
  ("Yield the values from " (arg iterator) " for which " (arg pred)
   " is non-" nil ".")
  :type (&-> (&* (&-> 'a bool) (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-remove (pred iterator)
      (declare (indent 1))
      (it-filter (lambda (elt) (not (pred elt))) iterator))
  :public t
  :summary
  ("Yield the values from " (arg iterator) " for which " (arg pred)
   " is " nil ".")
  :type (&-> (&* (&-> 'a bool) (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-non-nil (iterator)
      (iter-do (elt iterator)
        (when elt
          (iter-yield elt))))
  :public t
  :summary ("Yield the non-" nil " values from " (arg iterator) ".")
  :type (&-> (iterator (&? 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-keep (fn iterator)
      (let (result)
        (iter-do (elt iterator)
          (setq result (funcall fn elt))
          (when result
            (iter-yield result)))))
  :public t
  :summary
  ("Yield the non-" nil " results of applying " (arg fn) " to the elements of "
   (arg iterator) ".")
  :type (&-> (&* (&-> 'a (&? 'b)) (iterator 'a)) (iterator 'b)))

(doc-fun
    (iter-defun it-flatten (iterator)
      (iter-do (elt iterator)
        (iter-yield-from elt)))
  :public t
  :summary
  ("Convert " (arg iterator) ", an iterator of iterators, into a single "
   "iterator over all of their values.")
  :type (&-> (iterator (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-take (n iterator)
      (declare (indent 1))
      (dotimes (_ n)
        (iter-yield (iter-next iterator)))
      (iter-close iterator))
  :public t
  :summary ("Yield the first " (arg n) " elements of " (arg iterator) ".")
  :description
  ("If " (arg iterator) " yields fewer than " (arg n) " elements, "
   "all of them are yielded.")
  :type (&-> (&* integer (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-drop (n iterator)
      (declare (indent 1))
      (dotimes (_ n)
        (iter-next iterator))
      (iter-yield-from iterator))
  :public t
  :summary
  ("Yield all but the first " (arg n) " elements of " (arg iterator) ".")
  :description
  ("If " (arg iterator) " yields " (arg n) " or fewer elements, "
   "yields nothing.")
  :type (&-> (&* integer (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-take-while (pred iterator)
      (declare (indent 1))
      (let ((next (iter-next iterator)))
        (while (funcall pred next)
          (iter-yield next)
          (setq next (iter-next iterator))))
      (iter-close iterator))
  :public t
  :summary
  ("Yield successive values from the start of " (arg iterator) " for which "
   (arg pred) " returns non-" nil ".")
  :type (&-> (&* (&-> 'a bool) (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-drop-while (pred iterator)
      (declare (indent 1))
      (let ((next (iter-next iterator)))
        (while (funcall pred next)
          (setq next (iter-next iterator)))
        (iter-yield next))
      (iter-yield-from iterator))
  :public t
  :summary
  ("Drop successive values from the start of " (arg iterator) " for which "
   (arg pred) " returns non-" nil ".")
  :type (&-> (&* (&-> 'a bool) (iterator 'a)) (iterator 'a)))

(doc-fun
    (iter-defun it-zip-with (fn &rest iterators)
      (declare (indent 1))
      (condition-case nil
          (while t
            (iter-yield (apply fn (mapcar #'iter-next iterators))))
        (iter-end-of-sequence nil))
      (dolist (iterator iterators)
        (iter-close iterator)))
  :public t
  :summary ("Zip " (arg iterators) " together using " (arg fn) ".")
  :description
  ("Yields " (arg fn) " applied to the next elements from all of the "
   (arg iterators) ".")
  :type (&-> (&* (&-> (&* 'a 'b &dots) 'r) (iterator 'a) (iterator 'b) &dots)
             (iterator 'r)))
(doc-fun
    (defun it-zip (&rest iterators)
      (apply #'it-zip-with #'list iterators))
  :public t
  :summary
  ("Return an iterator of lists, where each list contains the next elements "
   "from all of the " (arg iterators) ".")
  :type (&-> (&* (iterator 'a) (iterator 'b) &dots)
             (iterator (&* 'a 'b &dots))))

(doc-fun
    (iter-defun it-chain (&rest iterators)
      (dolist (iterator iterators)
        (iter-yield-from iterator)))
  :public t
  :summary
  ("Yield all the values from each of the " (arg iterators)
   " one after another.")
  :type (&-> (&* (iterator 'a) (iterator 'b) &dots)
             (iterator (&| 'a 'b &dots))))

(doc-fun
    (iter-defun it-repeat (elt)
      (while t
        (iter-yield elt)))
  :public t
  :summary ("Return an iterator that yields " (arg elt) " forever.")
  :type (&-> 'a (iterator 'a)))

(doc-fun
    (iter-defun it-cycle (&rest elts)
      (dolist (elt (nconc elts elts))
        (iter-yield elt)))
  :public t
  :summary ("Return an iterator that cycles through " (arg elts) " forever.")
  :type (&-> (&* 'a 'b &dots) (iterator (&| 'a 'b &dots))))

(doc-fun
    (iter-defun it-yield (&rest elts)
      (dolist (elt elts)
        (iter-yield elt)))
  :public t
  :summary ("Return an iterator that yields " (arg elts) ".")
  :type (&-> (&* 'a 'b &dots) (iterator (&| 'a 'b &dots))))

(doc-fun
    (iter-defun it-iterate (fn init)
      (let ((next init))
        (while t
          (iter-yield next)
          (setq next (funcall fn next)))))
  :public t
  :summary
  ("Produce an iterator whose " (meta-var n) "th element is " (arg fn)
   " applied to " (arg init) " " (meta-var n) " times.")
  :description
  ("That is, it yields " (arg init) ", then " `(,(arg fn) ,(arg init)) ", then "
   `(,(arg fn) (,(arg fn) ,(arg init))) ", etc.")
  :type (&-> (&* (&-> 'a 'a) 'a) (iterator 'a)))

(doc-fun
    (iter-defun it-unfold (fn seed)
      (let ((continue t))
        (while continue
          (pcase (funcall fn seed)
            ('nil (setq continue nil))
            (`(,val . ,new-seed)
             (setq seed new-seed)
             (iter-yield val))))))
  :public t
  :summary ("Produce an iterator from " (arg seed) " using " (arg fn) ".")
  :description
  ((arg fn) " should return " nil " to stop iteration, or a cons cell of the "
   "format " `(,(meta-var val) . ,(meta-var new-seed)) ", where "
   (meta-var val) " will be the next value yielded by the iterator and "
   (meta-var new-seed) "will be the new seed passed to " (arg fn) ".")
  :type (&-> (&* (&-> 'b (&? (&\. 'a 'b))) 'b) (iterator 'a)))

(doc-fun
    (defun it-some (pred iterator)
      (let (test)
        (condition-case nil
            (while (not test)
              (setq test (funcall pred (iter-next iterator))))
          (iter-end-of-sequence nil))
        test))
  :public t
  :summary
  ("Return " `(,(arg pred) ,(meta-var elt)) " for the first " (meta-var elt)
   " yielded by " (arg iterator) " where " `(,(arg pred) ,(meta-var elt))
   " is non-" nil ".")
  :type (&-> (&* (&-> 'a (&? 'b)) (iterator 'a)) (&? 'b)))

(doc-fun
    (defun it-reduce-from (fn init iterator)
      (let ((acc init))
        (iter-do (elt iterator)
          (setq acc (funcall fn acc elt)))
        acc))
  :public t
  :summary
  ("Reduce " (arg iterator) " using " (arg fn) " starting from " (arg init) ".")
  :description
  ("Applies " (arg fn) " to " (arg init) " and the first element of "
   (arg iterator) ", then applies " (arg fn) " to that value and the next "
   "element of " (arg iterator) ", and so on, returning the final result. If "
   (arg iterator) " yields no elements, returns " (arg init) ".\n\n"

   "If " (meta-var elt1) ", " (meta-var elt2) ", ..., " (meta-var eltn)
   " are the elements of " (arg iterator) ", this computes "
   `(,(arg fn) (,"..." (,(arg fn) (,(arg fn) ,(arg init) ,(meta-var elt1))
                        ,(meta-var elt2)) ,"...") ,(meta-var eltn)))
  :type (&-> (&* (&-> (&* 'b 'a) 'b) 'b (iterator 'a)) 'b))

(doc-fun
    (defun it-reduce (fn iterator)
      (condition-case nil
          (let ((init (iter-next iterator)))
            (it-reduce-from fn init iterator))
        (iter-end-of-sequence (funcall fn))))
  :public t
  :summary
  ("Reduce " (arg iterator) " using " (arg fn) ".")
  :description
  ("Applies " (arg fn) " to the first two elements of " (arg iterator)
   ", then applies " (arg fn) " to that value and the next element of "
   (arg iterator) ", and so on, returning the final result. If "
   (arg iterator) " has only one element, simply returns that element. If it "
   "has no elements, returns the result of calling " (arg fn)
   " with no arguments.\n\n"

   "If " (meta-var elt1) ", " (meta-var elt2) ", " (meta-var elt3) ", ..., "
   (meta-var eltn) " are the elements of " (arg iterator) ", this computes "
   `(,(arg fn) (,"..." (,(arg fn) (,(arg fn) ,(arg elt1) ,(meta-var elt2))
                        ,(meta-var elt3)) ,"...") ,(meta-var eltn)))
  :type (&-> (&* (&-> (&* 'a 'a) 'a) (iterator 'a)) 'a))


(provide 'it)
;;; it.el ends here
