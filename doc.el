;;; doc.el --- Better docstrings -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Rose Kunkel
;;
;; Author: Rose Kunkel
;; Maintainer: Rose Kunkel <rose@rosekunkel.me>
;; Created: March 10, 2021
;; Modified: March 10, 2021
;; Version: 0.1.0
;; Keywords: docs extensions lisp
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; This package implements two main ideas: structured documentation and
;; structured docstrings.
;;
;; Structured documentation represents documentation as a plist of different
;; types of documentation, such as the one-line summary, examples, and relevant
;; info pages. This structured representation can then be used in a variety of
;; ways. This package supports several use cases out of the box:
;; - generating docstrings
;; - generating a markdown api reference
;; - running examples as tests
;;
;; In addition to structured documentation, this package provides structured
;; docstrings, a semantic way to write docstrings. This structure can be
;; exploited in order to e.g. generate html with links to the documentation of
;; mentioned symbols.
;;
;;; Code:

(defvar doc--as-docstring-helpers
  `((arg . ,#'doc--metavar-as-docstring)
    (meta-var . ,#'doc--metavar-as-docstring)
    (url . ,#'doc--docstring-url)
    (info-node . ,#'doc--info-as-docstring)
    (key . ,#'doc--key-as-docstring)
    (keymap . ,#'doc--keymap-as-docstring)
    (variable . ,(apply-partially #'doc--symbol-as-docstring "variable"))
    (option . ,(apply-partially #'doc--symbol-as-docstring "option"))
    (function . ,(apply-partially #'doc--symbol-as-docstring "function"))
    (command . ,(apply-partially #'doc--symbol-as-docstring "command"))
    (symbol . ,(apply-partially #'doc--symbol-as-docstring "symbol"))
    (program . ,(apply-partially #'doc--symbol-as-docstring "program"))))

(defun doc--metavar-as-docstring (name)
  (upcase (doc--escape-docstring (if (symbolp name) (symbol-name name) name))))

(defun doc--url-as-docstring (url)
  (concat "URL `" (doc--escape-docstring url) "'"))

(defun doc--info-as-docstring (first-part &optional second-part)
  (concat "Info node `"
          (doc--escape-docstring
           (if second-part
               (concat "(" first-part ")" second-part)
             first-part))
          "'"))

(defun doc--key-as-docstring (command &optional keymap)
  (concat (when keymap `("\\<" ,(symbol-name keymap) ">"))
          "\\[" (symbol-name command) "]"))

(defun doc--keymap-as-docstring (keymap)
  (concat "\\{" (symbol-name keymap) "}"))

(defun doc--symbol-as-docstring (prefix name)
  (concat prefix " `" (doc--escape-docstring (if (symbolp name) (symbol-name name) name)) "'"))

(defun doc-format-type (type &optional level)
  (unless level (setq level 0))
  (let (string atomic)
    (pcase type
      ('&dots
       (setq string "..."
             atomic t))
      (`(&-> ,t1 ,t2)
       (setq string (concat (doc-format-type t1 (1+ level)) " -> "
                            (doc-format-type t2 (1+ level)))))
      (`(&| . ,ts)
       (setq string (mapconcat (lambda (ty) (doc-format-type ty (1+ level)))
                               ts " | ")))
      (`(&* . ,ts)
       (setq string (mapconcat (lambda (ty) (doc-format-type ty (1+ level)))
                               ts " * ")))
      (`(&. ,t1 ,t2)
       (setq string (concat "(" (doc-format-type t1 (1+ level)) " . "
                            (doc-format-type t2 (1+ level)) ")")
             atomic t))
      (`(&? ,ty)
       (setq string (concat "?" (doc-format-type ty (1+ level)))
             atomic t))
      ((and `(&: ,name ,ty) (guard (symbolp name)))
       (setq string (concat "(" (symbol-name name) ": "
                            (doc-format-type ty 0) ")")
             atomic t))
      (`(quote . ,_)
       (setq string (format "%S" type)
             atomic t))
      (`(,constructor . ,ts)
       (setq string
             (concat
              (doc-format-type constructor (1+ level))
              " "
              (mapconcat (lambda (ty) (doc-format-type ty (1+ level)))
                         ts " "))))
      ((pred symbolp)
       (setq string (symbol-name type)
             atomic t))
      ((pred stringp) (setq string type))
      (_ (error "Not a valid type annotation: %S" type)))
    (if (and (> level 0) (not atomic))
        (concat "(" string ")")
      string)))

(defun doc--docstring-part-as-docstring (part)
  (pcase part
    ('t "t")
    ('nil "nil")
    ((pred stringp) (doc--escape-docstring part))
    ((pred symbolp) (concat "`" (doc--escape-docstring (symbol-name part)) "'"))
    (`(quote ,expr) (doc--escape-docstring (format "%S" expr)))
    ((or ``,_ `(backquote ,_))
     (doc--docstring-as-docstring `(,(macroexpand part))))

    ;; We get lists when we expand backquotes
    (`(list . ,rest)
     (concat "(" (mapconcat #'doc--docstring-part-as-docstring rest " ") ")"))
    (`(cons ,p1 ,p2)
     (concat "(" (doc--docstring-part-as-docstring p1) " . "
             (doc--docstring-part-as-docstring p2) ")"))
    ((and `(,ref . ,args)
          (guard (symbolp ref)))
     (let ((helper (cdr-safe (assq ref doc--as-docstring-helpers))))
       (if helper
           (apply helper args)
         (error "Not a known reference type: %S" part))))
    (_ (error "Not a valid part of a structured docstring: %S" part))))

(defun doc--fill-docstring (docstring)
  (with-temp-buffer
    (when (integerp emacs-lisp-docstring-fill-column)
      (setq fill-column emacs-lisp-docstring-fill-column))
    (insert docstring)
    (fill-region (point-min) (point-max))
    (buffer-string)))

(defun doc--docstring-as-docstring (docstring)
  (if (stringp docstring)
      docstring
    (mapconcat #'doc--docstring-part-as-docstring docstring "")))

(defun doc--escape-docstring (string)
  (setq
   string (replace-regexp-in-string (regexp-quote "\\") "\\=\\" string t t)
   string (replace-regexp-in-string (regexp-quote "`") "\\=`" string t t)
   string (replace-regexp-in-string (regexp-quote "'") "\\='" string t t)))

(defun doc-as-docstring (doc)
  (let ((docstring ""))
    (cl-flet ((docstring-push (&rest strings)
                              (setq docstring
                                    (apply #'concat docstring strings)))
              (render-info-node (node)
                                (if (stringp node)
                                    (doc--info-as-docstring node)
                                  (apply #'doc--info-as-docstring node))))
      (let ((summary (plist-get doc :summary)))
        (docstring-push (doc--docstring-as-docstring summary) "\n"))
      (when-let ((description (plist-get doc :description)))
        (docstring-push (doc--fill-docstring (doc--docstring-as-docstring description)) "\n\n"))
      (when-let ((type (plist-get doc :type)))
        (docstring-push (doc--escape-docstring (doc-format-type type)) "\n\n"))
      (when-let ((info (plist-get doc :info)))
        (pcase info
          ((or (pred stringp) `(,(pred stringp) . ,(pred stringp)))
           (docstring-push "For more information, see "
                           (render-info-node info) "\n"))
          ((or `(,(pred stringp)) `((,(pred stringp) . ,(pred stringp))))
           (docstring-push "For more information, see "
                           (render-info-node (car info)) "n"))
          (_ (docstring-push "For more information, see:\n")
             (dolist (node info)
               (docstring-push "- " (render-info-node node) "\n"))
             (docstring-push "\n"))))
      (when-let ((examples (plist-get doc :examples)))
        (docstring-push "Examples:\n")
        (dolist (example examples)
          (let ((code (plist-get example :code)))
            (docstring-push (doc--escape-docstring
                             (concat "```\n" code "\n```\n"))))
          (when-let ((result (plist-get example :result)))
            (docstring-push "Result:\n"
                            (doc--escape-docstring (pp-to-string result)) "\n"))
          (when-let ((output (plist-get example :output)))
            (docstring-push "Output:\n" (doc--escape-in-docstring output) "\n"))
          (docstring-push "\n")))
      (when-let ((calling-convention (plist-get doc :calling-convention)))
        (doc--escape-docstring
         (if (stringp calling-convention)
             "(fn " calling-convention ")"
             (format "%S" (cons 'fn calling-convention))))))
    (setq docstring (replace-regexp-in-string "\\`[ \t\n\r]+\\|[ \t\n\r]+\\'" ""
                                              docstring t t))
    docstring))

(defun doc--build-markdown (doc)
  `((header ,(plist-get doc :name) (code ,(plist-get doc args)))
    (code ,(plist-get doc :type))))

(defun doc-as-markdown (doc)
  (doc--render-markdown (doc--build-markdown doc)))

(defmacro doc-fun (form &rest props)
  (let ((name (make-symbol "name"))
        (doc (make-symbol "doc"))
        (args (make-symbol "args")))
    `(let* ((,name ,form)
            (,args (help-function-arglist ,name t))
            (,doc ',props))
       (plist-put ,doc :name (or (plist-get ,doc :name) ,name))
       (plist-put ,doc :args (or (plist-get ,doc :args) ,args))
       (put ,name 'doc-function-documentation ,doc)
       (put ,name 'function-documentation (doc-as-docstring ,doc)))))

(eval-after-load 'doc
  (doc-fun
      :summary ("Document " (arg name) " with the structured documentation "
                "properties " (arg props) ".")
      doc-fun))

(doc-as-docstring
 '(:visibility private
   :summary "Abc"
   :description "Some `more' info"
   :type (&-> (&* (&: res symbol) integer (iterator (iterator (&-> 'a 'b))) &dots) res)
   :info ("Some node" ("another node" "a manual"))
   :examples
   ((:code "(setq a 2)" :result 2))
   :calling-convention (one two &rest three)))

(provide 'doc)
;;; doc.el ends here
