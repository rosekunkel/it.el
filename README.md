# it.el

Iterators for Emacs.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [it.el](#itel)
    - [Installation](#installation)
    - [API](#api)
        - [Converting to and from iterators](#converting-to-and-from-iterators)
            - [(it-from obj)](#it-from-obj)
            - [(it-collect type iterator)](#it-collect-type-iterator)
        - [Transforming iterators](#transforming-iterators)
            - [(it-map fn iterator)](#it-map-fn-iterator)
            - [(it-flat-map fn iterator)](#it-flat-map-fn-iterator)
            - [(it-filter pred iterator)](#it-filter-pred-iterator)
            - [(it-flatten iterator)](#it-flatten-iterator)
            - [(it-take n iterator)](#it-take-n-iterator)
            - [(it-drop n iterator)](#it-drop-n-iterator)
            - [(it-take-while pred iterator)](#it-take-while-pred-iterator)
            - [(it-drop-while pred iterator)](#it-drop-while-pred-iterator)
        - [Combining iterators](#combining-iterators)
            - [(it-zip &rest iterators)](#it-zip-rest-iterators)
            - [(it-zip-with fn &rest iterators)](#it-zip-with-fn-rest-iterators)
            - [(it-chain &rest iterators)](#it-chain-rest-iterators)
        - [Creating new iterators](#creating-new-iterators)
            - [(it-repeat elt)](#it-repeat-elt)
            - [(it-iterate fn init)](#it-iterate-fn-init)
            - [(it-unfold fn seed)](#it-unfold-fn-seed)
    - [Changelog](#changelog)

<!-- markdown-toc end -->

## Installation

<!-- TODO: Add to MELPA. -->

Put `it.el` in your load path.

## API

Type annotations are provided for intuition but are not enforced.

### Converting to and from iterators

#### (it-from obj)
`a -> iterator b`

#### (it-collect type iterator) 
`(t: symbol) * iterator a -> t a`

### Transforming iterators

#### (it-map fn iterator)
`(a -> b) * iterator a -> iterator b`

#### (it-flat-map fn iterator)
`(a -> iterator b) * iterator a -> iterator b`

#### (it-filter pred iterator)
`(a -> bool) * iterator a -> iterator a`

#### (it-flatten iterator)
`iterator (iterator a) -> iterator a`

#### (it-take n iterator)
`integer * iterator a -> iterator a`

#### (it-drop n iterator)
`integer * iterator a -> iterator a`

#### (it-take-while pred iterator)
`(a -> bool) * iterator a -> iterator a`

#### (it-drop-while pred iterator)
`(a -> bool) * iterator a -> iterator a`

### Combining iterators

#### (it-zip &rest iterators)
`iterator a * iterator b * ... -> iterator (a * b * ...)`

#### (it-zip-with fn &rest iterators)
`(a * b * ... -> r) * iterator a * iterator b * ... -> iterator r`

#### (it-chain &rest iterators)
`iterator a * iterator a * ... -> iterator a`

### Creating new iterators

#### (it-repeat elt)
`a -> iterator a`

#### (it-iterate fn init)
`(a -> a) -> a -> iterator a`

#### (it-unfold fn seed)
`(a -> optional (b . a)) -> a -> iterator b`

## Changelog

it.el uses semantic versioning, so a breaking change will result in the major
version increasing (once we reach 1.0.0). See [CHANGELOG.md](CHANGELOG.md) for a
history of all changes.
